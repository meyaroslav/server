const fastify = require('fastify');
const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const server = fastify();

server.register(require('@fastify/cors'), {
    origin: '*',
    methods: ['GET', 'POST', 'PUT', 'DELETE'],
});

server.get('/news', async (request, reply) => {
    try {
        const news = await prisma.news.findMany();
        reply.send(news);
    } catch (error) {
        reply.status(500).send({ error: 'An error occurred while fetching news.' });
    }
});

server.post('/news', async (request, reply) => {
    try {
        const { title, content } = request.body;
        const news = await prisma.news.create({
            data: {
                title,
                content,
            },
        });
        reply.send(news);
    } catch (error) {
        reply.status(500).send({ error: 'An error occurred while creating news.' });
    }
});

server.put('/news/:id', async (request, reply) => {
    try {
        const { id } = request.params;
        const { title, content } = request.body;
        const news = await prisma.news.update({
            where: {
                id,
            },
            data: {
                title,
                content,
            },
        });
        reply.send(news);
    } catch (error) {
        reply.status(500).send({ error: 'An error occurred while updating news.' });
    }
});

server.delete('/news/:id', async (request, reply) => {
    try {
        const { id } = request.params;
        const news = await prisma.news.delete({
            where: {
                id,
            },
        });
        reply.send(news);
    } catch (error) {
        reply.status(500).send({ error: 'An error occurred while deleting news.' });
    }
});

const start = async () => {
    try {
        await prisma.$connect();
        await server.listen(3000);
        console.log('Server is running on http://localhost:3000');
    } catch (error) {
        console.error('Error starting server:', error);
        process.exit(1);
    }
};

start();